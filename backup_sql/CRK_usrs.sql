-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: mysql.cba.pl
-- Czas wygenerowania: 24 Cze 2012, 23:47
-- Wersja serwera: 5.1.63-1-log
-- Wersja PHP: 5.3.14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `fr_testsite_cba_pl`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `CRK_usrs`
--

CREATE TABLE IF NOT EXISTS `CRK_usrs` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `usr` varchar(255) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `pwd` varchar(255) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `adm` int(1) NOT NULL DEFAULT '0',
  `strt` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usr` (`usr`),
  FULLTEXT KEY `autostart` (`strt`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Zrzut danych tabeli `CRK_usrs`
--

INSERT INTO `CRK_usrs` (`id`, `usr`, `pwd`, `adm`, `strt`) VALUES
(1, 'wolf2789', '7c25a1094bb2d1702b94eef00d3468a6', 1, '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
