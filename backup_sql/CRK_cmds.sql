-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: mysql.cba.pl
-- Czas wygenerowania: 24 Cze 2012, 23:44
-- Wersja serwera: 5.1.63-1-log
-- Wersja PHP: 5.3.14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `fr_testsite_cba_pl`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `CRK_cmds`
--

CREATE TABLE IF NOT EXISTS `CRK_cmds` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `vis` tinyint(1) NOT NULL DEFAULT '1',
  `cmd` varchar(255) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `desc` text CHARACTER SET utf8 COLLATE utf8_polish_ci,
  `eval` text CHARACTER SET utf8 COLLATE utf8_polish_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cmd` (`cmd`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Zrzut danych tabeli `CRK_cmds`
--

INSERT INTO `CRK_cmds` (`id`, `vis`, `cmd`, `desc`, `eval`) VALUES
(1, 1, '!?', 'about', 'out(''<br/><center style="background:#0f0;color:#000;font-size:30px;"><br/><h1><font style="color:#0ff;background:#000;">''.str_repeat(''&nbsp;'',13).''<br/>&nbsp;Cr4k3N&nbsp;Sh3LL&nbsp;<br/>''.str_repeat(''&nbsp;'',13).''</font></h1><br/><font style="color:#000;background:#0ff;">&nbsp;&nbsp;Type "?" for more info.&nbsp;&nbsp;</font><br/><br/></center>'');'),
(2, 1, '?', 'display this text', '$txt=''<br/><table style="border:0;background:#000;color:#0ff;"><tr><td>&nbsp;&nbsp;<td>Command&nbsp;<td>Description'';$rslt=mysql_query("SELECT * FROM CRK_cmds ORDER BY id");if($rslt)while($cmd=mysql_fetch_assoc($rslt))if($cmd[''vis'']==''1'')$txt.=''<tr><td><td style="border:#0f0 1px solid;padding:3px;">''.$cmd[''cmd''].''<td style="border:#0f0 1px solid;padding:3px;">''.($cmd[''desc'']==""?"no description":$cmd[''desc'']);$txt.=''</table>'';out($txt);'),
(3, 1, 'cls', 'clear output', 'out(''<script>CRK_output[CRK_cur_tab]="$ cls<br/>";oS();</script>'');'),
(4, 1, '<', 'display specified text<br/>if no text after "<" - show if input display is enabled or disabled<br/>if text after "<" equals:<br/>&nbsp;&nbsp;<0> - disable input display<br/>&nbsp;&nbsp;<1> - enable input display<br/>&nbsp;&nbsp;<> - toggle input display', 'if(sizeof($input[1])==0)echo"echo(''Output display is currently ''+(CRK_echo?''enabled'':''disabled'')+''.<br/>'')";elseif($input[1][0]==''<0>''){echo"CRK_echo=false;";out(''Output display disabled.'');}elseif($input[1][0]==''<1>''){echo"CRK_echo=true;";out(''Output display enabled.'');}elseif($input[1][0]==''<>''){echo"CRK_echo=!CRK_echo;";out(''Output display toggled.'');}else{$txt='''';foreach($input[1] as $a)$txt.='' ''.$a;out($txt);}'),
(7, 0, 'usr', '', 'if($input[1][0]=="+"){\n if(mysql_query("SELECT * FROM CRK_usrs WHERE usr=''".$input[1][1]."''",$CRK_mysql_con))out("Usr (''".$input[1][1]."'') already exists!");\n else{\n  mysql_query("INSERT INTO CRK_usrs (usr,pwd,adm) VALUES (''".$input[1][1]."'',''".CRK_md5(CRK_xor($input[1][2],$input[1][1]),2)."'',0)",$CRK_mysql_con);\n  out("Added usr ".$input[1][1].".");\n }\n}else{\n if($usr=$mysql_fetch_assoc(mysql_query("SELECT * FROM CRK_usrs WHERE usr=''".$input[1][0]."''",$CRK_mysql_con)))\n if((CRK_md5($input[1][1],2)==$usr[''pwd''])||(CRK_xor($input[1][1],$input[1][0])==$usr[''pwd''])||(CRK_md5(CRK_xor($input[1][1],$input[1][0]),2)==$usr[''pwd''])){\n  $_SESSION[''usr'']=$usr[''usr''];\n  $_SESSION[''adm'']=$usr[''adm''];\n  out("Usr logged in as ''".$input[1][0]."''");\n }else out("Bad password.");\n}'),
(5, 1, 'js', 'execute javascript code', 'for($i=0;$i<sizeof($input[1]);$i++)echo$input[1][$i]." ";'),
(6, 0, 'php', 'execute php code', '$evl="";for($i=0;$i<sizeof($input[1]);$i++)$evl.=$input[1][$i]." ";eval($evl);');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
